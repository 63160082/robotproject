/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.robotproject;

/**
 *
 * @author ACER
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection;
    
    public Robot (int x , int y , int bx , int by , int N) {
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    
    public boolean inMap(int x , int y) {
        if(x >= N || x < 0 || y >= N || y < 0){
            return false;
        }
        return  true;
    }
    
    public boolean Walk (char direction) {
        switch (direction) {
            case 'N' :
                if(!inMap(x , y-1)) {
                    printUnableMove();
                    return false;
                }
                y = y - 1;
                break;
            case 'S' :
                if(!inMap(x , y+1)) {
                    printUnableMove();
                    return false;
                }
                y = y + 1;
                break;
            case 'E' :
                if(!inMap(x+1 , y)) {
                    printUnableMove();
                    return false;
                }                
                x = x + 1;
                break; 
            case 'W' :
                if(!inMap(x-1 , y)) {
                    printUnableMove();
                    return false;
                }                  
                x = x - 1;
                break;                
        }
        
        lastDirection = direction;
        if(isBomb()) {
            printBomb();
        }
        return true;
    }
    
    public boolean Walk (char direction , int step) {
        for(int i = 0; i < step; i++) {
            if(!this.Walk(direction)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean Walk() {
        return this.Walk(lastDirection);
    }
    
    public boolean Walk(int step) {
        return this.Walk(lastDirection ,step);
    }
    
    public String toString() {
        return "Robot ( " + this.x + " , " + this.y + " )";
    }
    
    public void printUnableMove() {
        System.out.println("I Can;t Move !!!");
    }
    
    public void printBomb() {
        System.out.println("Bomb Found !!!");
    }
    
    public boolean isBomb() {
        if(x == bx && y == by) {
            return true;
        }
        return false;
    }
    

    
}
